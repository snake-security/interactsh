cd cmd/interactsh-client

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip interactsh-client

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf interactsh-client /opt/ANDRAX/bin/interactsh

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
